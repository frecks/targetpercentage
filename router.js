/**
 * Created by afreckelton on 11/12/2014.
 */
module.exports = function(router) {
  router.get('/targetpercentage/:name', function(req, res) {
    var route = {
      req: req,
      res: res
    };

    require('./targetpercentage')(route);
  });

  router.use('/', function(req, res) {
    var html = 'To find data on the number of wins to 50% in each of your tanks, try this url (substituting your name for furgal).' +
      '<br /><a href="/targetpercentage/furgal?percentage=50">/targetpercentage/furgal?percentage=50</a>';
    res.send(html);
  });

  return router;
};