/**
 * Created by afreckelton on 11/12/2014.
 */
module.exports = function($) {
  function formatStr(format) {
    var args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] != 'undefined' ? args[number] : match;
    });
  }

  function getLowTanks(tanks) {
    return tanks.filter(function(tank) {
      return tank.winsReq > 0;
    }).sort(function(tank1, tank2) {
      return tank1.winsReq - tank2.winsReq;
    });
  }

  function calcWinsReq(battles, rate) {
    var wins = Math.round(battles*(rate/100));
    var winsReq=battles-(wins*2);
    return winsReq;
  }

  function mapRowToTank(row){
    var name, battles, rate;
    name = $(row).find('.t-profile_armory-icon').find('.b-name-vehicle').text();
    battles = parseInt($(row).find('.t-profile_right').text(), 10);
    rate = parseInt($(row).find('.t-profile_center').text().replace(/%/, ''), 10);

    return {
      name: name,
      battles: battles,
      rate: rate,
      winsReq: calcWinsReq(battles, rate)
    };
  }

  function logData(tanks) {
    var i, lowTank, str, lowTanks;
    lowTanks = getLowTanks(tanks);
    str = '<table><thead><tr><th>Wins to 50%</th><th style="text-align: left;padding-left: 10px;">Tank</th></tr></thead><tbody>';
    for(i=0; i < lowTanks.length; i++) {
      lowTank = lowTanks[i];
      str += formatStr('<tr><td>{0}</td><td style="padding-left: 10px;">{1}</td></tr>', lowTank.winsReq, lowTank.name);
    }
    str += formatStr('</tbody></table><br />{0} of {1} tanks below 50% win rate.', lowTanks.length, tanks.length);
    str += formatStr('<br />{0} wins required to achieve 50%+ win rate in every tank.', lowTanks.reduce(function(winsReq, tank) {
      return winsReq + tank.winsReq;
    }, 0));
    return str;
  }

  return (function () {
    var tanks;
    tanks = $('.t-profile_tankstype__item').get().map(mapRowToTank);
    return logData(tanks);
  }());
};