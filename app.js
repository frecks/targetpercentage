/**
 * Created by afreckelton on 11/12/2014.
 */
var router, express, app;

express = require('express');
router = require('./router')(express.Router());
app = express();

//Set the port port that the app listens on to the environment variable, or 5000 if the environment variable is undefined.
app.listen(process.env.PORT || 5000);

//Set the router function for the app to use.
app.use('/', router);