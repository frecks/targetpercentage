/**
 * Created by Andrew on 11/23/2014.
 */
var request, cheerio;

request = require('request');
cheerio = require('cheerio');

//I don't remember why I needed this.
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

/*Uncomment this to allow using Fiddler to examine network traffic.
request = request.defaults({
  jar: true,
  proxy: 'http://127.0.0.1:8889'
});*/

module.exports = function(route) {
  function getAccountUrl(callback) {
    //Get the URL for the given account, then execute the callback function with it as a parameter.
    var req = {
      uri: 'http://worldoftanks.com/community/accounts/search/?offset=0&limit=25&order_by=name&search=' + route.req.params.name,
      headers: {
        'Host': 'worldoftanks.com',
        'X-Requested-With': 'XMLHttpRequest'
      }
    };

    request(req, function(err, res, body) {
      if(err) {
        console.log(err);
        return;
      }
      callback(JSON.parse(body).request_data.items[0].account_url);
    });
  }

  function getVehicleData(accountUrl) {
    //Get the vehicle data from the account url, then call the scrape function to format it before sending it back
    //to the user in the response.
    var req = {
      uri: 'http://worldoftanks.com' + accountUrl,
      headers: {
        'Host': 'worldoftanks.com',
        'X-Requested-With': 'XMLHttpRequest'
      }
    };

    request(req, function(err, res, body) {
      if(err) {
        console.log(err);
        return;
      }
      var $ = cheerio.load(body);
      route.res.send(require('./scrape')($));
    });
  }

  getAccountUrl(getVehicleData);
};